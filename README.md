# utils

#### 项目介绍
项目工具类，以后在此项目中添加功能也尽可能是添加那些依赖少的，公共的东西
目前拥有如下功能:
- 压缩解压缩算法策略
- 时间处理
- 事件机制
- 配置文件按环境加载
- redis接口
- 统一的报文响应
- 路由算法
- 敏感词查找和过滤
- 序列化和反序列化接口
- servlet工具类
- 线程池监控
- 其他工具


### 使用方式
下载本项目，gradle clean build得到的jar包引入工程即可。

或者使用gradle或者maven引入，最新为utils:1.8.34。
1.8.6之前支持从jcenter依赖。从1.8.6开始，仅支持jitpack.io[![](https://jitpack.io/v/com.gitee.xxssyyyyssxx/utils.svg)](https://jitpack.io/#com.gitee.xxssyyyyssxx/utils)

```gradle
compile ("top.jfunc.common:utils:${version}")
```
```maven
<dependency>
    <groupId>top.jfunc.common</groupId>
    <artifactId>utils</artifactId>
    <version>${version}</version>
</dependency>
```

或者jitpack.io


```gradle
maven { url 'https://jitpack.io' }
compile 'com.gitee.xxssyyyyssxx:utils:v1.8.6'
```