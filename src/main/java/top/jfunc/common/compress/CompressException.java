package top.jfunc.common.compress;

/**
 * @author xiongshiyan at 2022/5/26 , contact me with email yanshixiong@126.com or phone 15208384257
 */
public class CompressException extends Exception{
    public CompressException(){}
    public CompressException(String message){super(message);}
    public CompressException(Exception e){super(e);}
    public CompressException(String message, Exception e){super(message, e);}
}
