package top.jfunc.common.compress;

import java.io.InputStream;

/**
 * 压缩解压接口
 * @author xiongshiyan at 2022/5/26 , contact me with email yanshixiong@126.com or phone 15208384257
 */
public interface Compressor {
    /**
     * 压缩
     * @param src 源
     * @return 压缩后的字节数组
     * @throws CompressException CompressException
     */
    default byte[] compress(byte[] src) throws CompressException{
        return compress(src, 0, src.length);
    }
    /**
     * 压缩，从指定位置压缩指定长度
     * @param src 源
     * @param offset 偏移量
     * @param length 长度
     * @return 压缩后的字节数组
     * @throws CompressException CompressException
     */
    byte[] compress(byte[] src, int offset, int length) throws CompressException;

    /**
     * 可以在某些情况下优化代码：不用字符串再拼接一次，增加内存和CPU消耗
     * @param src 源
     * @param more 源1,2,3
     * @return 压缩后的字节数组
     * @throws CompressException CompressException
     */
    byte[] compress(byte[] src, byte[]...more) throws CompressException;

    /**
     * 对流进行压缩
     * @param src 源
     * @return 压缩后的字节数组
     * @throws CompressException CompressException
     */
    byte[] compress(InputStream src) throws CompressException;

    /**
     * 解压
     * @param src 压缩的直接数组
     * @return 解压后的直接数组
     * @throws CompressException CompressException
     */
    default byte[] unCompress(byte[] src) throws CompressException{
        return unCompress(src, 0, src.length);
    }
    /**
     * 解压，从指定位置解压指定长度
     * @param src 压缩的直接数组
     * @param offset 偏移量
     * @param length 长度
     * @return 解压后的直接数组
     * @throws CompressException CompressException
     */
    byte[] unCompress(byte[] src, int offset, int length) throws CompressException;

    /**
     * 对流进行解压
     * @param src 压缩的直接数组
     * @return 解压后的直接数组
     * @throws CompressException CompressException
     */
    byte[] unCompress(InputStream src) throws CompressException;
}
