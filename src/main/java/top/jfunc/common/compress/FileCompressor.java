package top.jfunc.common.compress;

import java.io.File;

/**
 *
 * @author xiongshiyan at 2022/5/26 , contact me with email yanshixiong@126.com or phone 15208384257
 */
public interface FileCompressor {
    /**
     * 压缩
     * @param src 源文件或者文件夹
     * @param dest 压缩后的文件
     */
    void compress(File src, File dest) throws CompressException;

    /**
     * 解压
     * @param src 压缩文件
     * @param dest 解压后的目录
     */
    void unCompress(File src, File dest) throws CompressException;
}
