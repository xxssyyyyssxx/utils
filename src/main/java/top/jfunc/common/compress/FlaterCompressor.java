package top.jfunc.common.compress;

import top.jfunc.common.utils.ArrayUtil;
import top.jfunc.common.utils.IoUtil;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.zip.Deflater;
import java.util.zip.DeflaterInputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

/**
 * deflate算法实现压缩解压
 */
public class FlaterCompressor implements Compressor{
    private int bufferSize = IoUtil.DEFAULT_BUFFER_SIZE;
    /**
     * compression level (0-9)
     */
    private int level = Deflater.DEFAULT_COMPRESSION;

    public FlaterCompressor(){
    }
    public FlaterCompressor(int bufferSize, int deflaterLevel){
        this.bufferSize = bufferSize;
        this.level = deflaterLevel;
    }


    @Override
    public byte[] compress(byte[] src, int offset, int length) throws CompressException {
        Deflater deflater = null;
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            deflater = new Deflater(level);
            deflater.setInput(src, offset, length);
            deflater.finish();

            byte[] bytes = new byte[bufferSize];
            while (!deflater.finished()){
                int count = deflater.deflate(bytes);
                out.write(bytes,0, count);
            }
            return out.toByteArray();
        }catch (Exception e){
            throw new CompressException(e);
        }
        finally {
            if(null != deflater){
                deflater.end();
            }
        }

    }

    @Override
    public byte[] compress(byte[] src, byte[]... more) throws CompressException {
        byte[] mergedBytes = ArrayUtil.merge(src, more);
        return compress(mergedBytes);

        /*try (DeflaterInputStream in = new DeflaterInputStream(new ByteArrayInputStream(bytes));
             ByteArrayOutputStream out = new ByteArrayOutputStream()){
            IoUtil.copy(in,out);

            return out.toByteArray();
        }catch (Exception e){
            throw new CompressException(e);
        }*/
    }

    @Override
    public byte[] compress(InputStream src) throws CompressException {
        try (DeflaterInputStream in = new DeflaterInputStream(src);
             ByteArrayOutputStream out = new ByteArrayOutputStream()){
            IoUtil.copy(in,out);
            return out.toByteArray();
        }catch (Exception e){
            throw new CompressException(e);
        }
    }

    @Override
    public byte[] unCompress(byte[] src, int offset, int length) throws CompressException {
        Inflater inflater = null;
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            inflater = new Inflater();
            inflater.setInput(src, offset, length);

            byte[] bytes = new byte[bufferSize];
            while (!inflater.finished()){
                int count = inflater.inflate(bytes);
                out.write(bytes,0, count);
            }
            return out.toByteArray();
        }catch (Exception e){
            throw new CompressException(e);
        }
        finally {
            if(null != inflater){
                inflater.end();
            }
        }
    }

    @Override
    public byte[] unCompress(InputStream src) throws CompressException {
        try (InflaterInputStream in = new InflaterInputStream(src);
             ByteArrayOutputStream out = new ByteArrayOutputStream()){
            IoUtil.copy(in,out);
            return out.toByteArray();
        }catch (Exception e){
            throw new CompressException(e);
        }
    }

    @Override
    public String toString(){
        return getClass().getName();
    }
}
