package top.jfunc.common.datetime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;

/**
 * 时间起止
 */
public class TimeRangeBean {
	private final LocalDateTime from;
	private final LocalDateTime to;

	public TimeRangeBean(LocalDateTime from, LocalDateTime to) {
		this.to = to;
		this.from = from;
	}

	/**
	 * 从此刻算起过去一个月【零点】
	 * 例如： 2024-03-22 00:00:00---2024-04-22 00:00:00
	 */
	public static TimeRangeBean passMonth() {
		LocalDateTime to = LocalDateTime.now().withHour(0).withMinute(0).withSecond(0).withNano(0);
		LocalDateTime from = to.minusMonths(1);
		return new TimeRangeBean(from, to);
	}

	/**
	 * 上一个月【零点】
	 * 例如：2024-03-01 00:00:00---2024-03-31 00:00:00
	 */
	public static TimeRangeBean lastMonth() {
		LocalDate lastMonth = LocalDate.now().minusMonths(1);
		LocalDate from = lastMonth.with(TemporalAdjusters.firstDayOfMonth());
		LocalDate to = lastMonth.with(TemporalAdjusters.lastDayOfMonth());
		return new TimeRangeBean(LocalDateTime.of(from, LocalTime.MIN), LocalDateTime.of(to, LocalTime.MIN));
	}

	/**
	 * 这一个月的第一天到最后一天
	 * 例如： 2024-04-01 00:00:00---2024-04-30 00:00:00
	 */
	public static TimeRangeBean thisMonth() {
		LocalDate lastMonth = LocalDate.now();
		LocalDate from = lastMonth.with(TemporalAdjusters.firstDayOfMonth());
		LocalDate to = lastMonth.with(TemporalAdjusters.lastDayOfMonth());
		return new TimeRangeBean(LocalDateTime.of(from, LocalTime.MIN), LocalDateTime.of(to, LocalTime.MIN));
	}
	/**
	 * 这一个月的第一天到今天的零点
	 * 例如： 2024-04-01 00:00:00---2024-04-22 00:00:00
	 */
	public static TimeRangeBean thisMonthNow() {
		LocalDate now = LocalDate.now();
		LocalDate from = now.with(TemporalAdjusters.firstDayOfMonth());
		return new TimeRangeBean(LocalDateTime.of(from, LocalTime.MIN), LocalDateTime.of(now, LocalTime.MIN));
	}

	/**
	 * 昨日0点到最后一秒
	 * 2024-04-21 00:00:00---2024-04-21 23:59:59
	 */
	public static TimeRangeBean lastDay() {
		LocalDate now = LocalDate.now().minusDays(1);
		return new TimeRangeBean(LocalDateTime.of(now, LocalTime.MIN), LocalDateTime.of(now, LocalTime.MAX));
	}
	/**
	 * 今日日0点到最后一秒
	 * 2024-04-22 00:00:00---2024-04-22 23:59:59
	 */
	public static TimeRangeBean thisDay() {
		LocalDate now = LocalDate.now();
		return new TimeRangeBean(LocalDateTime.of(now, LocalTime.MIN), LocalDateTime.of(now, LocalTime.MAX));
	}

	/**
	 * 当前时间的整点-1 ~ 当前时间的整点，比如2024年1月1日13:10:00计算就是2024年1月1日12:00:00 ~ 2024年1月1日13:00:00
	 * 例如： 2024-04-22 16:00:00---2024-04-22 17:00:00
	 */
	public static TimeRangeBean integerPassHour() {
		LocalDateTime to = LocalDateTime.now().withMinute(0).withSecond(0).withNano(0);
		LocalDateTime from = to.minusHours(1);
		return new TimeRangeBean(from, to);
	}

	/**
	 * 过去一分钟
	 * 例如： 2024-04-22 17:28:00---2024-04-22 17:29:00
	 */
	public static TimeRangeBean integerPassMinute() {
		LocalDateTime to = LocalDateTime.now().withSecond(0).withNano(0);
		LocalDateTime from = to.minusMinutes(1);
		return new TimeRangeBean(from, to);
	}

	/**
	 * 给定一个时间，计算其整点20分钟
	 * 例如： 2024-04-22 17:00:00---2024-04-22 17:20:00
	 */
	public static TimeRangeBean integerPass20Minute(LocalDateTime dateTime) {
		return integerPassMinute(dateTime, 20);
	}
	public static TimeRangeBean integerPassMinute(LocalDateTime dateTime, int minute) {
		dateTime = dateTime.withSecond(0).withNano(0);

		/*if(dateTime.getMinute()<20){
			dateTime = dateTime.withMinute(0);
		}else if(dateTime.getMinute()<40){
			dateTime = dateTime.withMinute(20);
		}else {
			dateTime = dateTime.withMinute(40);
		}*/
		dateTime = dateTime.withMinute(dateTime.getMinute() / minute * minute);
		LocalDateTime from = dateTime.minusMinutes(minute);
		return new TimeRangeBean(from, dateTime);
	}










	public LocalDateTime getFrom() {
		return from;
	}

	public String getFromAsString(String pattern) {
		return from.format(DateTimeFormatter.ofPattern(pattern));
	}
	public String getFromAsString(DateTimeFormatter formatter) {
		return from.format(formatter);
	}
	public LocalDateTime getTo() {
		return to;
	}

	public String getToAsString(String pattern) {
		return to.format(DateTimeFormatter.ofPattern(pattern));
	}
	public String getToAsString(DateTimeFormatter formatter) {
		return to.format(formatter);
	}

}
