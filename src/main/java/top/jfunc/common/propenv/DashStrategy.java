package top.jfunc.common.propenv;

import top.jfunc.common.utils.FileUtil;
import top.jfunc.common.utils.StrUtil;

/**
 * ${fileName}-${env}.ext
 * @author xiongshiyan at 2021/2/7 , contact me with email yanshixiong@126.com or phone 15208384257
 */
public class DashStrategy implements EnvFileNameStrategy{
    @Override
    public String generate(String srcFileName, String env) {
        if(StrUtil.isEmpty(env)){
            return srcFileName;
        }
        return FileUtil.getFileNameNoEx(srcFileName) + "-" + env +
                (srcFileName.contains(".") ? ("." + FileUtil.getExtensionName(srcFileName)) : "");
    }
}
