package top.jfunc.common.propenv;

/**
 * 给定环境变量及原文件名，产生相关的文件名
 * @author xiongshiyan at 2021/2/7 , contact me with email yanshixiong@126.com or phone 15208384257
 */
public interface EnvFileNameStrategy {
    /**
     * 产生env file name
     * @param srcFileName 原文件名
     * @param env 环境
     * @return 产生的文件名
     */
    String generate(String srcFileName, String env);
}
