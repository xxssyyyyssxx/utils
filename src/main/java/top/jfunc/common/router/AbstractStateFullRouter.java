package top.jfunc.common.router;

import java.util.List;

/**
 * 根据服务器状态决定选择的，可衍生出故障转移和忙碌转移等
 */
public abstract class AbstractStateFullRouter<T,O> implements Router<T,O> {

    @Override
    public T select(List<T> toSelectedList, O otherParam) {
        for (T t : toSelectedList) {
            try {
                //状态正常就选择
                if(stateSuccess(toSelectedList, otherParam)){
                    return t;
                }
            } catch (Exception e) {
                //ignore
            }
        }
        return null;
    }

    protected abstract boolean stateSuccess(List<T> toSelectedList, O otherParam) throws Exception;
}
