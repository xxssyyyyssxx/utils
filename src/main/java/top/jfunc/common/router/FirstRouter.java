package top.jfunc.common.router;

import java.util.List;

/**
 * 选中第一个
 */
public class FirstRouter<T,O> implements Router<T,O> {

    @Override
    public T select(List<T> toSelectedList, O otherParam) {
        return toSelectedList.get(0);
    }
}
