package top.jfunc.common.router;

import java.util.List;

/**
 * 选中最后一个
 */
public class LastRouter<T,O> implements Router<T,O> {

    @Override
    public T select(List<T> toSelectedList, O otherParam) {
        return toSelectedList.get(toSelectedList.size()-1);
    }
}
