package top.jfunc.common.router;

import java.util.List;
import java.util.Random;

/**
 * 随机选中一个
 */
public class RandomRouter<T,O> implements Router<T,O> {

    private static final Random localRandom = new Random();

    @Override
    public T select(List<T> toSelectedList, O otherParam) {
        return toSelectedList.get(localRandom.nextInt(toSelectedList.size()));
    }
}
