package top.jfunc.common.router;

import java.util.List;

/**
 * 路由规则接口
 * @param <T>
 */
public interface Router<T,O> {
    /**
     * 从待选列表中选择一个
     * @param toSelectedList 待选列表
     * @param otherParam 其他参数，方便某些算法的时候传参
     * @return 选中的
     */
    T select(List<T> toSelectedList, O otherParam);
}
