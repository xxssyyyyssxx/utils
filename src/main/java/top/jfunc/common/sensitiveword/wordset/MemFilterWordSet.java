package top.jfunc.common.sensitiveword.wordset;

import top.jfunc.common.sensitiveword.FilterWordSet;

import java.util.HashSet;
import java.util.Set;

/**
 * 敏感词保存在内存中
 * @author 熊诗言
 */
public class MemFilterWordSet implements FilterWordSet {

    private Set<String> set = new HashSet<>();

    public MemFilterWordSet(Set<String> set) {
        this.set = set;
    }

    public MemFilterWordSet() {
    }

    public void add(String word){
        set.add(word);
    }
    public void addAll(Set<String> words){
        set.addAll(words);
    }

    @Override
    public Set<String> getWordSet(){
        return set;
    }

}
