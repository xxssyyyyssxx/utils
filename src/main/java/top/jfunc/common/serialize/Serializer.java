package top.jfunc.common.serialize;

/**
 * 序列化和反序列化接口
 */
public interface Serializer {
    /**
     * 序列化
     * @param object 待序列化对象
     * @return 序列化字节数组
     */
    byte[] serialize(Object object);

    /**
     * 反序列化
     * @param bytes 待反序列化字节数组
     * @param clazz 反序列化对象类
     * @return 反序列化对象
     * @param <T> 泛型
     */
    <T> Object deserialize(byte[] bytes, Class<T> clazz);
}
