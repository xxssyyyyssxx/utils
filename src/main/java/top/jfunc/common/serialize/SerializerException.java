package top.jfunc.common.serialize;

public class SerializerException extends RuntimeException{
    public SerializerException(){}

    public SerializerException(String message, Exception e){
        super(message, e);
    }
}
