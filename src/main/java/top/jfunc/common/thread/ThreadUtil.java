package top.jfunc.common.thread;

/**
 * @author xiongshiyan at 2018/5/15
 */
public class ThreadUtil {
    /**
     * 暂停毫秒
     * @param ms 毫秒
     */
    public static void sleep(long ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
        }
    }
    /**
     * 暂停秒
     * @param s 秒
     */
    public static void sleeps(long s){
        sleep(1000 * s);
    }



    /**
     * 暂停毫秒
     * 针对在循环中使用{@link Thread#isInterrupted()}判断线程是否结束的方式，需要重新设置中断标志位，因为{@link Thread#sleep(long)}会清除中断标志位
     * @param ms 毫秒
     */
    public static void sleepInterruptibly(long ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
    /**
     * 暂停秒
     * @param s 秒
     */
    public static void sleepsInterruptibly(long s){
        sleepInterruptibly(1000 * s);
    }
}
