package top.jfunc.common.thread.monitor;

/**
 * 可自适应修改参数的线程池接口
 * 上报数据后调用
 * @see top.jfunc.common.thread.monitor.change.ParamChangeStrategy
 * @see ExecutorMonitorService#reportMonitorData()
 */
public interface AutoChangeParamExecutorMonitorData extends ConfigurableExecutorMonitorData{
	void doChange();
}
