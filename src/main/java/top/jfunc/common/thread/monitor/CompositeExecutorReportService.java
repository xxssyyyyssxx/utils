package top.jfunc.common.thread.monitor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 组合service，系统中可能需要多种处理
 */
public class CompositeExecutorReportService implements ExecutorReportService{
	private final List<ExecutorReportService> reportServices = new ArrayList<>();

	public CompositeExecutorReportService(List<ExecutorReportService> reportServices) {
		this.reportServices.addAll(reportServices);
	}
	public CompositeExecutorReportService(ExecutorReportService... reportServices) {
		addServices(reportServices);
	}
	public CompositeExecutorReportService(ExecutorReportService executorReportService, ExecutorReportService... otherServices) {
		addServices(executorReportService);
		addServices(otherServices);
	}

	public CompositeExecutorReportService addServices(ExecutorReportService... reportServices){
		this.reportServices.addAll(Arrays.asList(reportServices));
		return this;
	}

	public List<ExecutorReportService> getReportServices() {
		return this.reportServices;
	}

	@Override
	public void initReport(ExecutorMonitorData executorMonitorData) {
		for (ExecutorReportService reportService :this.reportServices){
			reportService.initReport(executorMonitorData);
		}
	}

	@Override
	public void report(ExecutorMonitorData executorMonitorData) {
		for (ExecutorReportService reportService : this.reportServices) {
			reportService.report(executorMonitorData);
		}
	}
}
