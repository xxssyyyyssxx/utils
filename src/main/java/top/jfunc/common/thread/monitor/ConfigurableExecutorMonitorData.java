package top.jfunc.common.thread.monitor;

import top.jfunc.common.thread.monitor.change.ParamChangeBean;

/**
 * 可修改参数的线程池接口
 */
public interface ConfigurableExecutorMonitorData extends ExecutorMonitorData{
	/**
	 * 修改线程池参数
	 * @param bean 待修改的参数值
	 */
	void onChange(ParamChangeBean bean);
}
