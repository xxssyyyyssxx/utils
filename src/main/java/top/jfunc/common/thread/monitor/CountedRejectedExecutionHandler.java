package top.jfunc.common.thread.monitor;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 可统计拒绝次数的{@link RejectedExecutionHandler}
 */
public class CountedRejectedExecutionHandler implements RejectedExecutionHandler {
	private final RejectedExecutionHandler delegate;
	private final AtomicInteger counter = new AtomicInteger(0);

	public CountedRejectedExecutionHandler(RejectedExecutionHandler delegate) {
		this.delegate = delegate;
	}

	@Override
	public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
		counter.getAndIncrement();
		this.delegate.rejectedExecution(r,executor);
	}

	public int getRejectedCount() {
		return counter.get();
	}

	public void reset(){
		counter.set(0);
	}
	public RejectedExecutionHandler getRejectedExecutionHandler() {
		return delegate;
	}
}
