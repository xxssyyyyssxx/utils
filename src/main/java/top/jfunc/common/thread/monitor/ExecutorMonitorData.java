package top.jfunc.common.thread.monitor;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 定义监控数据接口
 */
public interface ExecutorMonitorData {
	/**
	 * 线程池启动时间
	 */
	Date getStartTime();

	/**
	 * 线程池所在服务
	 */
	String getService();

	/**
	 * 线程池名字，每个线程池有一个唯一的名字，比如HttpRequest、DbExecutor
	 */
	String getPoolName();

	/**
	 * 线程池的唯一标识，因为线程池可能因为重启等因素被关闭，但是要唯一区别一个线程池，所以不能使用{@link ExecutorMonitorData#getPoolName()}，
	 * 建议的格式为{@link top.jfunc.common.thread.monitor.generator.CommonIdentifierGenerator}
	 */
	String getIdentifier();

	/**
	 * 当前核心线程数
	 */
	int getPoolSize();

	/**
	 * 配置的核心线程数
	 */
	int getCorePoolSize();

	/**
	 * 历史到达最大线程数
	 */
	int getLargestPoolSize();

	/**
	 * 配置的最大线程数
	 */
	int getMaximumPoolSize();

	/**
	 * 当前活跃线程数
	 */
	int getActiveCount();

	/**
	 * 线程最大空闲时间
	 */
	long getKeepAliveTime(TimeUnit timeUnit);

	/**
	 * 是否允许核心线程空闲回收
	 */
	boolean allowsCoreThreadTimeOut();

	/**
	 * 当前任务数
	 */
	long getTaskCount();

	/**
	 * 已完成任务数
	 */
	long getCompletedTaskCount();

	/*BlockingQueue<Runnable> getQueue();

	ThreadFactory getThreadFactory();

	RejectedExecutionHandler getRejectedExecutionHandler();*/

    /**
     * 阻塞队列容量
     */
	int getQueueCapacity();

    /**
     * 阻塞队列当前数量
     */
	int getQueueSize();

    /**
     * 阻塞队列的类名
     */
	String getQueueClass();
    /**
     * 线程工厂的类名
     */
	String getThreadFactoryClass();
    /**
     * 拒绝策略的类名
     */
	String getRejectedExecutionHandlerClass();

	/**
	 * 任务执行总时间，依赖于{@link top.jfunc.common.thread.monitor.adapter.MonitoredThreadPoolExecutor}和{@link StatisticsRunnable}
	 */
	long getTotalExecuteTime();
    /**
     * 任务执行最小时间，依赖于{@link top.jfunc.common.thread.monitor.adapter.MonitoredThreadPoolExecutor}和{@link StatisticsRunnable}
     */
    long getMinExecuteTime();
    /**
     * 任务执行最大时间，依赖于{@link top.jfunc.common.thread.monitor.adapter.MonitoredThreadPoolExecutor}和{@link StatisticsRunnable}
     */
    long getMaxExecuteTime();
    /**
	 * 任务执行超时次数，依赖于{@link top.jfunc.common.thread.monitor.adapter.MonitoredThreadPoolExecutor#setExecuteTimeout(long)}和{@link StatisticsRunnable}
	 */
	int getExecuteTimeoutCount();
	/**
	 * 执行发生异常次数
	 */
	int getExecuteExceptionCount();

	/**
     * 任务排队最小时间，依赖于{@link top.jfunc.common.thread.monitor.adapter.MonitoredThreadPoolExecutor}和{@link StatisticsRunnable}
     */
    long getMinQueueTime();
	/**
     * 任务排队最大时间，依赖于{@link top.jfunc.common.thread.monitor.adapter.MonitoredThreadPoolExecutor}和{@link StatisticsRunnable}
     */
    long getMaxQueueTime();
	/**
	 * 任务执行超时次数，依赖于{@link top.jfunc.common.thread.monitor.adapter.MonitoredThreadPoolExecutor#setQueueTimeout(long)}和{@link StatisticsRunnable}
	 */
	int getQueueTimeoutCount();

	/**
	 * 获取拒绝次数
	 */
	int getRejectedCount();


	/**
	 * 重置一些统计数据，使统计数据具备统计周期的效果
	 */
	void resetStatistic();
}
