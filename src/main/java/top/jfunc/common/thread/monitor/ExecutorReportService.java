package top.jfunc.common.thread.monitor;

/**
 * 初始化线程池的时候调用，上报线程初始化相关信息
 */
public interface ExecutorReportService {
    /**
     * 初始化的时候上报一次
     * @param executorMonitorData 被监控线程池监控信息
     */
    void initReport(ExecutorMonitorData executorMonitorData);
    /**
     * 普通上报信息，定时或者手动触发
     * @param executorMonitorData 被监控线程池监控信息
     */
    void report(ExecutorMonitorData executorMonitorData);
}
