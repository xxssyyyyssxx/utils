package top.jfunc.common.thread.monitor;

/**
 * 可统计时间的runnable
 */
public class StatisticsRunnable implements Runnable {

    private final Runnable runnable;

    /**
     * 入队时间，一般也就是生成时间
     */
    private final long submitTime;

    /**
     * 开始执行时间
     */
    private long startTime;

    public StatisticsRunnable(Runnable runnable) {
        this.runnable = runnable;
        this.submitTime = System.currentTimeMillis();
    }

    @Override
    public void run() {
        runnable.run();
    }

    public long getSubmitTime() {
        return submitTime;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }
}
