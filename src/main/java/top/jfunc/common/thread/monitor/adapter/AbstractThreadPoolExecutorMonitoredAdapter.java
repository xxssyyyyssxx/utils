package top.jfunc.common.thread.monitor.adapter;

import java.util.Date;
import java.util.concurrent.ThreadPoolExecutor;

import top.jfunc.common.thread.monitor.AutoChangeParamExecutorMonitorData;
import top.jfunc.common.thread.monitor.change.ParamChangeStrategy;
import top.jfunc.common.thread.monitor.generator.CommonIdentifierGenerator;
import top.jfunc.common.thread.monitor.generator.IdentifierGenerator;

/**
 * 代理线程池，例如{@link ThreadPoolExecutor}，附加监控相关信息
 * 后续扩展此类，可以适配tomcat、jetty、undertow等
 */
public abstract class AbstractThreadPoolExecutorMonitoredAdapter implements AutoChangeParamExecutorMonitorData {
    /**
     * 所属服务
     */
    private final String service;
    /**
     * 线程池名称
     */
    private final String poolName;
    /**
     * 线程池启动时间
     */
    private final Date startTime;
    /**
     * 唯一标识
     */
    private String identifier;
    /**
     * 自动修改参数策略接口
     */
    private ParamChangeStrategy paramChangeStrategy;

    public AbstractThreadPoolExecutorMonitoredAdapter(String service, String poolName) {
        this.service = service;
        this.poolName = poolName;
        this.startTime = new Date();
        this.identifier = new CommonIdentifierGenerator().getIdentifier(this);
    }

    @Override
    public Date getStartTime(){
        return this.startTime;
    }
    @Override
    public String getPoolName() {
        return this.poolName;
    }

    @Override
    public String getService() {
        return this.service;
    }

    @Override
    public long getTotalExecuteTime() {
        //无法监控到执行时间，返回特定标识
        return 0;
    }

    @Override
    public long getMinExecuteTime() {
        //无法监控到执行时间，返回特定标识
        return 0;
    }

    @Override
    public long getMaxExecuteTime() {
        //无法监控到执行时间，返回特定标识
        return 0;
    }

    @Override
    public int getExecuteTimeoutCount() {
        //无法监控到执行时间，返回特定标识
        return 0;
    }

    @Override
    public int getExecuteExceptionCount() {
        //无法监控到执行时间，返回特定标识
        return 0;
    }

    @Override
    public long getMinQueueTime() {
        //无法监控到执行时间，返回特定标识
        return 0;
    }

    @Override
    public long getMaxQueueTime() {
        //无法监控到执行时间，返回特定标识
        return 0;
    }

    @Override
    public int getQueueTimeoutCount() {
        //无法监控到执行时间，返回特定标识
        return 0;
    }

    @Override
    public void resetStatistic() {
        //无须做任何事情
    }

    @Override
    public String getIdentifier() {
        return this.identifier;
    }


    public AbstractThreadPoolExecutorMonitoredAdapter setIdentifierGenerator(IdentifierGenerator identifierGenerator) {
        this.identifier = identifierGenerator.getIdentifier(this);
        return this;
    }

    public ParamChangeStrategy getParamChangeStrategy() {
        return paramChangeStrategy;
    }

    public AbstractThreadPoolExecutorMonitoredAdapter setParamChangeStrategy(ParamChangeStrategy paramChangeStrategy) {
        this.paramChangeStrategy = paramChangeStrategy;
        return this;
    }

    @Override
    public void doChange() {
        if(null != paramChangeStrategy){
            paramChangeStrategy.doChange(this);
        }
    }
}
