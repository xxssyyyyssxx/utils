package top.jfunc.common.thread.monitor.adapter;

import top.jfunc.common.thread.monitor.CountedRejectedExecutionHandler;

import java.util.concurrent.RejectedExecutionHandler;

public class RejectedExecutionHandlerUtil {
    private RejectedExecutionHandlerUtil(){}

    public static String getOriginalRejectedExecutionHandlerClass(RejectedExecutionHandler handler){
        if(handler instanceof CountedRejectedExecutionHandler){
            return ((CountedRejectedExecutionHandler)handler).getRejectedExecutionHandler().getClass().getName();
        }
        return handler.getClass().getName();
    }
    public static int getRejectedCount(RejectedExecutionHandler handler){
        if(handler instanceof CountedRejectedExecutionHandler){
            return ((CountedRejectedExecutionHandler)handler).getRejectedCount();
        }
        return 0;
    }
    public static void resetRejectedCount(RejectedExecutionHandler handler){
        if(handler instanceof CountedRejectedExecutionHandler){
            ((CountedRejectedExecutionHandler)handler).reset();
        }
    }
}
