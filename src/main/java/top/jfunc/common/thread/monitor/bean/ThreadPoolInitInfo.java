package top.jfunc.common.thread.monitor.bean;

import top.jfunc.common.thread.monitor.ExecutorMonitorData;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class ThreadPoolInitInfo {
    //线程池所在服务
    private final String service;
    //线程池名称
    private final String poolName;
    //线程池唯一标识
    private final String identifier;
    //线程池核心线程数量
    private final int corePoolSize;
    //线程池中允许的最大线程数
    private final int maximumPoolSize;
    //线程最大空闲时间（毫秒）
    private final long keepAliveTime;
    //是否允许核心线程空闲回收，0和1，数字方便存储
    private final int allowsCoreThreadTimeOut;
    //阻塞队列容量
    private final int queueCapacity;
    private final String threadFactoryClass;
    private final String blockingQueueClass;
    private final String rejectedExecutionHandlerClass;
    //线程池启动时间
    private final Date startTime;

    public ThreadPoolInitInfo(String service,
                              String poolName,
                              String identifier,
                              int corePoolSize,
                              int maximumPoolSize,
                              long keepAliveTime,
                              int allowsCoreThreadTimeOut,
                              int queueCapacity,
                              String blockingQueueClass,
                              String threadFactoryClass,
                              String rejectedExecutionHandlerClass,
                              Date startTime) {
        this.service = service;
        this.poolName = poolName;
        this.identifier = identifier;
        this.corePoolSize = corePoolSize;
        this.maximumPoolSize = maximumPoolSize;
        this.keepAliveTime = keepAliveTime;
        this.allowsCoreThreadTimeOut = allowsCoreThreadTimeOut;
        this.queueCapacity = queueCapacity;
        this.blockingQueueClass = blockingQueueClass;
        this.threadFactoryClass = threadFactoryClass;
        this.rejectedExecutionHandlerClass = rejectedExecutionHandlerClass;
        this.startTime = startTime;
    }

    public static ThreadPoolInitInfo fromMonitorData(ExecutorMonitorData emd){
        return new ThreadPoolInitInfo(emd.getService(), emd.getPoolName(), emd.getIdentifier(),
                emd.getCorePoolSize(), emd.getMaximumPoolSize(),emd.getKeepAliveTime(TimeUnit.MILLISECONDS), emd.allowsCoreThreadTimeOut() ? 1 : 0,
                emd.getQueueCapacity(), emd.getQueueClass(), emd.getThreadFactoryClass(), emd.getRejectedExecutionHandlerClass(),
                emd.getStartTime());
    }

    public String getService() {
        return service;
    }

    public String getPoolName() {
        return poolName;
    }

    public String getIdentifier() {
        return identifier;
    }

    public int getCorePoolSize() {
        return corePoolSize;
    }

    public int getMaximumPoolSize() {
        return maximumPoolSize;
    }

    public long getKeepAliveTime() {
        return keepAliveTime;
    }

    public int getAllowsCoreThreadTimeOut() {
        return allowsCoreThreadTimeOut;
    }

    public int getQueueCapacity() {
        return queueCapacity;
    }

    public String getThreadFactoryClass() {
        return threadFactoryClass;
    }

    public String getBlockingQueueClass() {
        return blockingQueueClass;
    }

    public String getRejectedExecutionHandlerClass() {
        return rejectedExecutionHandlerClass;
    }

    public Date getStartTime() {
        return startTime;
    }
}