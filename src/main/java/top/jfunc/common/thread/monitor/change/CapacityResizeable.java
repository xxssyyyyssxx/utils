package top.jfunc.common.thread.monitor.change;

public interface CapacityResizeable {
	int getCapacity();
	void setCapacity(int capacity);
}
