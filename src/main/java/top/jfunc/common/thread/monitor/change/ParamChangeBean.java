package top.jfunc.common.thread.monitor.change;

/**
 * 修改线程池参数，目前支持以下参数的修改
 */
public class ParamChangeBean {
	//线程池标识
	private String identifier;
	//线程池核心线程数量
	private int corePoolSize;
	//线程池中允许的最大线程数
	private int maximumPoolSize;
	//线程最大空闲时间（毫秒）
	private long keepAliveTime;
	//是否允许核心线程空闲回收，0和1
	private int allowCoreThreadTimeOut;
	/**
	 * 任务队列容量（阻塞队列），需要配合{@link ResizableCapacityLinkedBlockingQueue}
	 */
	private int queueCapacity;

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public int getCorePoolSize() {
		return corePoolSize;
	}

	public void setCorePoolSize(int corePoolSize) {
		this.corePoolSize = corePoolSize;
	}

	public int getMaximumPoolSize() {
		return maximumPoolSize;
	}

	public void setMaximumPoolSize(int maximumPoolSize) {
		this.maximumPoolSize = maximumPoolSize;
	}

	public long getKeepAliveTime() {
		return keepAliveTime;
	}

	public void setKeepAliveTime(long keepAliveTime) {
		this.keepAliveTime = keepAliveTime;
	}

	public int getAllowCoreThreadTimeOut() {
		return allowCoreThreadTimeOut;
	}

	public void setAllowCoreThreadTimeOut(int allowCoreThreadTimeOut) {
		this.allowCoreThreadTimeOut = allowCoreThreadTimeOut;
	}

	public int getQueueCapacity() {
		return queueCapacity;
	}

	public void setQueueCapacity(int queueCapacity) {
		this.queueCapacity = queueCapacity;
	}
}
