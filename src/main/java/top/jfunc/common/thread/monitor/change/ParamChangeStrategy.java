package top.jfunc.common.thread.monitor.change;

import top.jfunc.common.thread.monitor.ConfigurableExecutorMonitorData;

/**
 * 修改参数的策略
 */
@FunctionalInterface
public interface ParamChangeStrategy {
	void doChange(ConfigurableExecutorMonitorData configurableExecutorMonitorData);
}
