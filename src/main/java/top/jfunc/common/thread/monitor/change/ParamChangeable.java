package top.jfunc.common.thread.monitor.change;

import java.util.concurrent.TimeUnit;

/**
 * 修改线程池参数
 */
public interface ParamChangeable {
	int getCorePoolSize();

	void setCorePoolSize(int corePoolSize);

	int getMaximumPoolSize();

	void setMaximumPoolSize(int maximumPoolSize);

	long getKeepAliveTime(TimeUnit timeUnit);

	void setKeepAliveTime(long keepAliveTime, TimeUnit timeUnit);

	boolean allowsCoreThreadTimeOut();

	void allowCoreThreadTimeOut(boolean allowCoreThreadTimeOut);

	void setCapacity(int queueCapacity);
}
