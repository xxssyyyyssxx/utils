package top.jfunc.common.thread.monitor.change;

import java.util.concurrent.TimeUnit;

public class ParamChangerUtil {
	private ParamChangerUtil(){}

	public static void paramChange(ParamChangeable paramChangeable, ParamChangeBean bean){
		//核心线程数
		int corePoolSize = bean.getCorePoolSize();
		if (corePoolSize != paramChangeable.getCorePoolSize()){
			paramChangeable.setCorePoolSize(corePoolSize);
		}

		//最大线程数
		int maximumPoolSize = bean.getMaximumPoolSize();
		if (maximumPoolSize != paramChangeable.getMaximumPoolSize()){
			paramChangeable.setMaximumPoolSize(maximumPoolSize);
		}

		//最大空闲时间
		long keepAliveTime = bean.getKeepAliveTime();
		if (keepAliveTime != paramChangeable.getKeepAliveTime(TimeUnit.MILLISECONDS)){
			paramChangeable.setKeepAliveTime(keepAliveTime, TimeUnit.MILLISECONDS);
		}

		//是否允许核心线程空闲回收
		int allowCoreThreadTimeOut = bean.getAllowCoreThreadTimeOut();
		paramChangeable.allowCoreThreadTimeOut(1 == allowCoreThreadTimeOut);

		//队列容量
		paramChangeable.setCapacity(bean.getQueueCapacity());
	}
}
