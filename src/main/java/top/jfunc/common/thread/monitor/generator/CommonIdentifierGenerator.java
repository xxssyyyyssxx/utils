package top.jfunc.common.thread.monitor.generator;

import top.jfunc.common.thread.monitor.ExecutorMonitorData;

/**
 * 格式： service@@poolName@@startTime
 */
public class CommonIdentifierGenerator implements IdentifierGenerator{
	@Override
	public String getIdentifier(ExecutorMonitorData data) {
		return data.getService() + IDENTIFIER_SEPARATOR +
				data.getPoolName() + IDENTIFIER_SEPARATOR +
				data.getStartTime().getTime();
	}
}
