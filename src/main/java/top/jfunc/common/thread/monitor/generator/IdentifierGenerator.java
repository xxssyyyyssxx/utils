package top.jfunc.common.thread.monitor.generator;

import top.jfunc.common.thread.monitor.ExecutorMonitorData;

public interface IdentifierGenerator {
    String IDENTIFIER_SEPARATOR = "@@";
	/**
	 * 获取线程池唯一标识
	 * @param executorMonitorData 线程池相关数据
	 * @return 标识
	 */
	String getIdentifier(ExecutorMonitorData executorMonitorData);
}
