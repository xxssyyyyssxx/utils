package top.jfunc.common.thread.monitor.generator;

import top.jfunc.common.thread.monitor.ExecutorMonitorData;
import top.jfunc.common.utils.RandomUtil;

/**
 * 格式： uuid
 */
public class UUIDGenerator implements IdentifierGenerator{
	@Override
	public String getIdentifier(ExecutorMonitorData data) {
		return RandomUtil.randomUUID();
	}
}
