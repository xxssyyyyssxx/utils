package top.jfunc.common.thread.monitor.generator;

import top.jfunc.common.thread.monitor.ExecutorMonitorData;
import top.jfunc.common.utils.IpUtil;

/**
 * 格式： service@@ip@@poolName@@startTime
 */
public class WithIpIdentifierGenerator implements IdentifierGenerator{
	@Override
	public String getIdentifier(ExecutorMonitorData data) {
		return data.getService() + IDENTIFIER_SEPARATOR +
				IpUtil.getLocalIp() + IDENTIFIER_SEPARATOR +
				data.getPoolName() + IDENTIFIER_SEPARATOR +
				data.getStartTime().getTime();
	}
}
