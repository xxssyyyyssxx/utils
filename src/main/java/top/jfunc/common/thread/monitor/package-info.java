package top.jfunc.common.thread.monitor;

/**
 * 线程池监控机制：
 *
 * 定义监控功能上报{@link top.jfunc.common.thread.monitor.ExecutorReportService}接口，使用方实现该接口定义如何处理监控指标
 *
 * 抽象监控数据接口{@link top.jfunc.common.thread.monitor.ExecutorMonitorData}
 *
 * 具体监控线程池方案有两种，通过{@link top.jfunc.common.thread.monitor.ExecutorMonitorService#register(top.jfunc.common.thread.monitor.ExecutorMonitorData...)}注册：
 *     1. 扩展定义{@link java.util.concurrent.ThreadPoolExecutor}附加监控信息{@link top.jfunc.common.thread.monitor.adapter.MonitoredThreadPoolExecutor}，在原来使用{@link java.util.concurrent.ThreadPoolExecutor}的地方换为{@link top.jfunc.common.thread.monitor.adapter.MonitoredThreadPoolExecutor}即可，
 *     2. 代理{@link java.util.concurrent.ThreadPoolExecutor}附加监控信息{@link top.jfunc.common.thread.monitor.adapter.ThreadPoolExecutorMonitoredAdapter}，可对原有已定义的线程池进行包装
 *     3. 扩展{@link top.jfunc.common.thread.monitor.adapter.AbstractThreadPoolExecutorMonitoredAdapter}附加监控信息，可以适配tomcat、jetty、undertow等
 *
 * {@link top.jfunc.common.thread.monitor.ExecutorMonitorService}定时上报注册的线程池的性能指标，也可以主动调用{@link top.jfunc.common.thread.monitor.ExecutorMonitorService#reportMonitorData()}临时上报一次
 *
 * 线程池的参数动态修改基于{@link top.jfunc.common.thread.monitor.ConfigurableExecutorMonitorData}，当系统收到修改请求的时候【一般有可动态通知参数变化的配置中心最好，否则就需要自己实现监听或者提供接口，针对集群情况一种解决方案是利用nginx的mirror功能】，调用{@link top.jfunc.common.thread.monitor.ExecutorMonitorService#changeParam(ParamChangeBean)}，
 * 通过{@link top.jfunc.common.thread.monitor.change.ParamChangeBean#getIdentifier()}找到需要修改参数的线程池，对其进行参数修改
 *
 * 为了适配多种不同的线程池，定义了{@link top.jfunc.common.thread.monitor.change.ParamChangeable}，通过装饰器模式达到修改的目的，例如{@link top.jfunc.common.thread.monitor.change.ThreadPoolExecutorParamChangeAdapter}
 *
 * 可修改参数通过{@link top.jfunc.common.thread.monitor.change.ParamChangeable}和{@link top.jfunc.common.thread.monitor.change.ParamChangeBean}规定
 */

import top.jfunc.common.thread.monitor.change.ParamChangeBean;