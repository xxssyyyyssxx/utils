package top.jfunc.common.transfer;

import java.util.List;
import java.util.function.Consumer;

@FunctionalInterface
public interface DataSectionHandler<T> extends Consumer<List<T>> {
}
