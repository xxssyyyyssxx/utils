package top.jfunc.common.transfer;

import java.util.List;

@FunctionalInterface
public interface DataSectionSupplier<T>{
	/**
	 * 意义明确的分页查询数据的方法
	 * 如果查询到没有数据了就返回null或者一个空集合
	 */
	List<T> paginate(int pageNumber, int pageSize);
}
