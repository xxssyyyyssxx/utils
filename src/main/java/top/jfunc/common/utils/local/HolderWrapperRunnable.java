package top.jfunc.common.utils.local;

import top.jfunc.common.utils.MapUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 在子线程中就可以通过{@link StringMapInfoHolderUtil}获取{@link ThreadLocal}变量
 */
public class HolderWrapperRunnable<T> implements Runnable {

    private final Runnable runnable;

    private final Map<String, String> map;

    public HolderWrapperRunnable(Runnable runnable) {
        this(runnable, true);
    }
    public HolderWrapperRunnable(Runnable runnable, boolean readonly) {
        this.runnable = runnable;
        if(readonly){
            //这种不会破坏主线程的map
            Map<String, String> localMap = StringMapInfoHolderUtil.getLocalMap();
            map = new HashMap<>(localMap.size());
            map.putAll(localMap);
        }else {
            map = StringMapInfoHolderUtil.getLocalMap();
        }
    }

    @Override
    public void run() {
        if(!MapUtil.isEmpty(this.map)){
            StringMapInfoHolderUtil.addMap2Local(this.map);
        }
        // 装饰器模式，执行run方法
        try {
            runnable.run();
        }finally {
            StringMapInfoHolderUtil.clear();
            this.map.clear();
        }
    }
    
}