package top.jfunc.common.utils.local;

public class InheritableInfoHolder<T> extends InfoHolder<T>{
    public InheritableInfoHolder() {
        super(new InheritableThreadLocal<>());
    }
}
