package top.jfunc.common.utils.local;

import org.slf4j.MDC;
import top.jfunc.common.utils.MapUtil;

import java.util.Map;

/**
 * 串联子线程和主线程的{@link MDC}数据
 */
public class MDCWrapperRunnable implements Runnable {

    private final Runnable runnable;

    private final Map<String, String> map;

    public MDCWrapperRunnable(Runnable runnable) {
        this.runnable = runnable;
        // 保存当前线程的MDC值
        this.map = MDC.getCopyOfContextMap();
    }

    @Override
    public void run() {
        if(!MapUtil.isEmpty(this.map)){
            this.map.forEach(MDC::put);
        }
        // 装饰器模式，执行run方法
        try {
            runnable.run();
        }finally {
            if(!MapUtil.isEmpty(this.map)){
                this.map.keySet().forEach(MDC::remove);
                this.map.clear();
            }
        }
    }
    
}