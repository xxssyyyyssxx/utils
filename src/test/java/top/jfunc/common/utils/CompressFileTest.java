package top.jfunc.common.utils;

import org.junit.Ignore;
import org.junit.Test;
import top.jfunc.common.compress.CompressException;
import top.jfunc.common.compress.FileCompressor;
import top.jfunc.common.compress.ZipFileCompressor;
import top.jfunc.common.thread.ThreadUtil;

/**
 * @author xiongshiyan at 2022/5/26 , contact me with email yanshixiong@126.com or phone 15208384257
 */
public class CompressFileTest {
    @Test@Ignore
    public void testZipFileCompressor() throws CompressException {
        testFileCompressor(new ZipFileCompressor());
    }

    void testFileCompressor(FileCompressor fileCompressor) throws CompressException {
        java.io.File xx = new java.io.File("C:\\Users\\xiongshiyan\\Desktop\\xx.zip");
        java.io.File yy = new java.io.File("C:\\Users\\xiongshiyan\\Desktop\\yy.zip");
        fileCompressor.compress(new java.io.File("C:\\Users\\xiongshiyan\\Desktop\\xxdir"), xx);
        fileCompressor.compress(new java.io.File("C:\\Users\\xiongshiyan\\Desktop\\yy.txt"), yy);

        ThreadUtil.sleep(1);

        fileCompressor.unCompress(xx,new java.io.File("C:\\Users\\xiongshiyan\\Desktop\\11"));
        fileCompressor.unCompress(yy,new java.io.File("C:\\Users\\xiongshiyan\\Desktop\\22\\"));
    }
}
