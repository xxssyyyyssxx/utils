package top.jfunc.common.utils;

import org.junit.Assert;
import org.junit.Test;
import top.jfunc.common.compress.*;

/**
 * @author xiongshiyan at 2022/5/26 , contact me with email yanshixiong@126.com or phone 15208384257
 */
public class CompressMoreTest {
    @Test
    public void testGzipMoreCompressor() throws CompressException {
        testMoreCompress(new GzipCompressor());
    }
    @Test
    public void testZipMoreCompressor() throws CompressException {
        testMoreCompress(new ZipCompressor());
    }
    @Test
    public void testFlaterMoreCompressor() throws CompressException {
        testMoreCompress(new FlaterCompressor());
    }
    void testMoreCompress(Compressor compressor) throws CompressException {
        String s1 = "xxx1";
        String s2 = "xxx2";
        String s3 = "xxx3";
        String s4 = "xxx4";
        String s5 = s1 + s2 + s3 + s4;
        byte[] bytes3 = s5.getBytes();
        byte[] compress1 = compressor.compress(s1.getBytes(), s2.getBytes(), s3.getBytes(), s4.getBytes());
        byte[] compress2 = compressor.compress(bytes3);

        Assert.assertEquals(compress1.length, compress2.length);

        for (int i = 0; i < compress1.length; i++) {
            Assert.assertEquals(compress1[i], compress2[i]);
        }

        byte[] bytes1 = compressor.unCompress(compress1);
        byte[] bytes2 = compressor.unCompress(compress2);
        for (int i = 0; i < bytes1.length; i++) {
            Assert.assertEquals(bytes1[i], bytes2[i]);
            Assert.assertEquals(bytes1[i], bytes3[i]);
        }

        Assert.assertEquals(new String(bytes1),s5);

    }
}
