package top.jfunc.common.utils;

import org.junit.Assert;
import org.junit.Test;
import top.jfunc.common.compress.*;

/**
 * @author xiongshiyan at 2022/5/26 , contact me with email yanshixiong@126.com or phone 15208384257
 */
public class CompressTest {
    @Test
    public void testGzipCompressor() throws CompressException {
        testCompress(new GzipCompressor());
    }
    @Test
    public void testZipCompressor() throws CompressException {
        testCompress(new ZipCompressor());
    }
    @Test
    public void testFlaterCompressor() throws CompressException {
        testCompress(new FlaterCompressor(1024,9));
    }

    void testCompress(Compressor compressor) throws CompressException {
        String src = "login_no=ase1X0027|send_time=20220526115759|login_name=杜蔓|phone_no=18802801611|channel_id=D0001|regionId=11\n";
        byte[] bytes = src.getBytes();
        //System.out.println(src.length());
        System.out.println("src length:" + bytes.length);
        byte[] compress = compressor.compress(bytes);
        System.out.println(compressor + ":" + compress.length);

        //这个还真不一定满足
        //Assert.assertTrue(compress.length<src.length());

        byte[] s = compressor.unCompress(compress);
        for (int i = 0; i < bytes.length; i++) {
            Assert.assertEquals(s[i], bytes[i]);
        }
    }
}
