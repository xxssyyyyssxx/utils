package top.jfunc.common.utils;

import org.databene.contiperf.PerfTest;
import org.databene.contiperf.Required;
import org.databene.contiperf.junit.ContiPerfRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import top.jfunc.common.thread.monitor.StatisticsRunnable;
import top.jfunc.common.thread.monitor.adapter.MonitoredStatisticsCounter;

public class CounterPerfTest {
	@Rule
	public ContiPerfRule rule = new ContiPerfRule();

	MonitoredStatisticsCounter counter = new MonitoredStatisticsCounter();

	@Before
	public void bef(){
		counter.setExecuteTimeout(1000);
		counter.setQueueTimeout(1000);
	}

	@Test
	@PerfTest(invocations = 1000, threads = 40)
	@Required(max = 500, average = 100, totalTime = 60000)
	public void testPerf(){
		StatisticsRunnable r = new StatisticsRunnable(() -> { });
		counter.beforeExecute(null, r);
		counter.afterExecute(r,new RuntimeException());
		counter.resetStatistic();
	}
}
