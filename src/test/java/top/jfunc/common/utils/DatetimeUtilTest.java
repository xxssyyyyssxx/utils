package top.jfunc.common.utils;

import org.junit.Test;
import top.jfunc.common.datetime.DatetimeUtil;

import java.text.ParseException;
import java.util.Date;

public class DatetimeUtilTest {
    @Test
    public void datetimeUtilTest() throws ParseException {
        System.out.println(DatetimeUtil.format(new Date(), "yyyy-MM-dd"));
        System.out.println(DatetimeUtil.parse("2022-11-02", "yyyy-MM-dd"));
    }
}
