package top.jfunc.common.utils;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

public class EnumUtilTest {
    enum Color{
        GREEN,RED,YELLOW
    }
    
    @Test
    public void testEnumUtil() {
        Assert.assertEquals(3, EnumUtil.matchIdentifier(Color.values()).length());
        Assert.assertEquals("111", EnumUtil.matchIdentifier(Color.values()));
        Assert.assertEquals("111", EnumUtil.matchIdentifier(Color.class));

        Assert.assertTrue(EnumUtil.matches("100", Color.GREEN));
        Assert.assertTrue(EnumUtil.matches("010", Color.RED));
        Assert.assertTrue(EnumUtil.matches("001", Color.YELLOW));

        Assert.assertTrue(EnumUtil.matches("110", new Color[]{Color.GREEN, Color.RED}));
        Assert.assertTrue(EnumUtil.matches("011", new Color[]{Color.YELLOW, Color.RED}));
        Assert.assertTrue(EnumUtil.matches("101", new Color[]{Color.GREEN, Color.YELLOW}));

        Assert.assertEquals("101",EnumUtil.matchIdentifier(Arrays.asList(Color.GREEN,Color.YELLOW)));
        Assert.assertEquals("010",EnumUtil.matchIdentifier(Color.class, new Color[]{Color.GREEN,Color.YELLOW}));

        AtomicInteger count = new AtomicInteger();
        EnumUtil.doIfMatch("111",Color.values(),(e)->{
            count.getAndIncrement();
        });
        Assert.assertEquals(count.get(), 3);

    }
}
