package top.jfunc.common.utils;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import top.jfunc.common.propenv.AbsolutePathEnvStream;
import top.jfunc.common.propenv.Prop;
import top.jfunc.common.propenv.PropertiesUtils;

import java.io.*;
import java.util.Map;

/**
 * @author xiongshiyan at 2021/2/7 , contact me with email yanshixiong@126.com or phone 15208384257
 */
public class EnvTest {
    @Ignore
    @Test
    public void testAbsolute() throws IOException {
        InputStream inputStream = new AbsolutePathEnvStream("C:\\Users\\xiongshiyan\\Desktop").loadEnvInputStream("xx.txt");
        Assert.assertTrue(null != inputStream);
    }
    @Ignore
    @Test
    public void testLoadStreamMap() throws IOException {
        Map<String, InputStream> map = new AbsolutePathEnvStream("C:\\Users\\xiongshiyan\\Desktop").loadEnvInputStreamMap("xx.txt");
        System.out.println(map.keySet());
    }
    @Ignore
    @Test
    public void testUseLikeSpring() throws IOException {
        AbsolutePathEnvStream envStream = new AbsolutePathEnvStream("C:\\Users\\xiongshiyan\\Desktop");
        Prop prop = PropertiesUtils.useLikeSpring("xx.txt", envStream);
        System.out.println(prop.toMap());
    }
}
