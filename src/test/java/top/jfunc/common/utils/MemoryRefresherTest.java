package top.jfunc.common.utils;

import org.junit.Test;
import top.jfunc.common.thread.ThreadUtil;

public class MemoryRefresherTest {
    @Test
    public void test(){
        MemoryRefresher<String> refresher = new MemoryRefresher<>(() -> "xx" + System.currentTimeMillis());
        refresher.setRefreshTimeout(5000);
        for (int i = 0; i < 100; i++) {
            System.out.println(refresher.getData());
            ThreadUtil.sleeps(1);
        }
    }
}
