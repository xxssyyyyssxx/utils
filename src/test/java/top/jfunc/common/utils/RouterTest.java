package top.jfunc.common.utils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import top.jfunc.common.router.*;

import java.util.ArrayList;
import java.util.List;

public class RouterTest {

    private List<String> addresses = new ArrayList<>();
    private Router<String,Object> routerFirst = new FirstRouter<>();
    private Router<String,Object> routerLast = new LastRouter<>();
    private Router<String,Object> routerRandom = new RandomRouter<>();
    private Router<String,Object> routerRoundRobin = new RoundRobinRouter<>();
    private Router<String,Object> routerConsistentHash = new ConsistentHashRouter<>();
    private Router<String,Object> routerLRU = new LRURouter<>();
    private Router<String,Object> routerLFU = new LFURouter<>();

    @Before
    public void init(){
        addresses.add("192.168.0.1");
        addresses.add("192.168.0.2");
        addresses.add("192.168.0.3");
        //addresses.add("192.168.0.4");
        //addresses.add("192.168.0.5");
        //addresses.add("192.168.0.6");
    }

    @Test
    public void testFirst(){
        for (int i = 0; i < 100; i++) {
            String select = routerFirst.select(addresses,null);
            Assert.assertEquals("192.168.0.1", select);
        }
    }
    @Test
    public void testLast(){
        for (int i = 0; i < 100; i++) {
            String select = routerLast.select(addresses,null);
            Assert.assertEquals("192.168.0.3", select);
        }
    }
    @Test
    public void testRandom(){
        for (int i = 0; i < 100; i++) {
            String select = routerRandom.select(addresses,null);
            System.out.println(select);
        }
    }
    @Test
    public void testRoundRobin(){
        for (int i = 0; i < 100; i++) {
            String select = routerRoundRobin.select(addresses,null);
            System.out.println(select);
        }
    }
    @Test
    public void testConsistentHash(){
        for (int i = 0; i < 100; i++) {
            String select = routerConsistentHash.select(addresses,null);
            System.out.println(select);
        }
    }
    @Test
    public void testLRU(){
        for (int i = 0; i < 100; i++) {
            String select = routerLRU.select(addresses,null);
            System.out.println(select);
        }
    }
    @Test
    public void testLFU(){
        for (int i = 0; i < 100; i++) {
            String select = routerLFU.select(addresses,null);
            System.out.println(select);
        }
    }

}
