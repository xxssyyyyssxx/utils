package top.jfunc.common.utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author xiongshiyan at 2019/3/5 , contact me with email yanshixiong@126.com or phone 15208384257
 */
public class StrUtilTest {
    @Test
    public void testToUnderlineCase(){
        Assert.assertEquals("hello_world" , StrUtil.toUnderlineCase("HelloWorld"));
        Assert.assertEquals("hello_world" , StrUtil.toUnderlineCase("helloWorld"));
        Assert.assertEquals("hello_world" , StrUtil.toUnderlineCase("helloWORLD"));
    }
    @Test
    public void testContainsEn_CN(){
        Assert.assertFalse(StrUtil.containsChinese("xsy"));
        Assert.assertTrue(StrUtil.containsChinese("言"));
        Assert.assertTrue(StrUtil.containsChinese("言x"));
        Assert.assertTrue(StrUtil.containsChinese("z言"));
        Assert.assertTrue(StrUtil.containsChinese("z言xx"));

        Assert.assertTrue(StrUtil.containsEnglish("xsy"));
        Assert.assertFalse(StrUtil.containsEnglish("言"));
        Assert.assertTrue(StrUtil.containsEnglish("x言"));
        Assert.assertTrue(StrUtil.containsEnglish("言z"));
        Assert.assertTrue(StrUtil.containsEnglish("z言xx"));
    }
}
