package top.jfunc.common.utils;

import org.junit.Test;

import java.time.LocalDateTime;

import top.jfunc.common.datetime.DatetimeUtils;
import top.jfunc.common.datetime.TimeRangeBean;

public class TimeRangeBeanTest {
    
    @Test
    public void testPassMonth() {
        TimeRangeBean timeRangeBean = TimeRangeBean.passMonth();

        print("passMonth", timeRangeBean);
    }
    @Test
    public void testLastMonth() {
        TimeRangeBean timeRangeBean = TimeRangeBean.lastMonth();

        print("lastMonth", timeRangeBean);
    }
    @Test
    public void testThisMonth() {
        TimeRangeBean timeRangeBean = TimeRangeBean.thisMonth();

        print("thisMonth",timeRangeBean);
    }
    @Test
    public void testThisMonthNow() {
        TimeRangeBean timeRangeBean = TimeRangeBean.thisMonthNow();

        print("thisMonthNow",timeRangeBean);
    }
    @Test
    public void testThisDay() {
        TimeRangeBean timeRangeBean = TimeRangeBean.thisDay();

        print("thisDay",timeRangeBean);
    }
    @Test
    public void testLastDay() {
        TimeRangeBean timeRangeBean = TimeRangeBean.lastDay();

        print("lastDay",timeRangeBean);
    }
    @Test
    public void integerPass20Minute() {
        TimeRangeBean timeRangeBean = TimeRangeBean.integerPass20Minute(LocalDateTime.now());

        print("integerPass20Minute",timeRangeBean);
    }
    @Test
    public void integerPassMinute() {
        TimeRangeBean timeRangeBean = TimeRangeBean.integerPassMinute();

        print("integerPassMinute",timeRangeBean);
    }
    @Test
    public void integerPassHour() {
        TimeRangeBean timeRangeBean = TimeRangeBean.integerPassHour();

        print("integerPassHour",timeRangeBean);
    }

    private void print(String prefix, TimeRangeBean timeRangeBean) {
        System.out.println(prefix + ":" + timeRangeBean.getFromAsString(DatetimeUtils.YYYY_MM_DD_HH_MM_SS) + "---" + timeRangeBean.getToAsString(DatetimeUtils.YYYY_MM_DD_HH_MM_SS));
    }
}
